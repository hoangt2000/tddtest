<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::delete('/posts/destroy/{id}', [\App\Http\Controllers\PostController::class, 'destroy'])
    ->name('posts.destroy')
    ->middleware('auth');
Route::get('/posts/{id}', [App\Http\Controllers\PostController::class, 'show'])
    ->name('posts.show')
    ->middleware('auth');
Route::put('/posts/update/{id}', [App\Http\Controllers\PostController::class, 'update'])
    ->name('posts.update')
    ->middleware('auth');
Route::get('/posts', [App\Http\Controllers\PostController::class, 'index'])
    ->name('posts.index')
    ->middleware('auth');
Route::post('/posts/store', [App\Http\Controllers\PostController::class, 'store'])
    ->name('posts.store')
    ->middleware('auth');
