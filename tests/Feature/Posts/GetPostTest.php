<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetPostTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_get_post()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $response = $this->get($this->getShowRoute($post->id));
        $response->assertViewIs('posts.show');
        $response->assertSee($post->name);
    }
    /** @test */
    public function unauthenticate_user_can_not_get_post()
    {
        $post = Post::factory()->create();
        $response = $this->get($this->getShowRoute($post->id));
        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticate_user_can_not_get_post_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $postId = -1;
        $response = $this->get($this->getShowRoute($postId));
        $response->assertStatus(404);
    }
    public function getShowRoute($id)
    {
        return route('posts.show', $id);
    }
}
