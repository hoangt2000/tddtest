<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListPostTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_get_list_post()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $response = $this->get($this->getIndexRoute());
        $response->assertViewIs('posts.index');
        $response->assertSee($post->name);

    }
    /** @test */
    public function unauthenticate_user_can_not_get_list_post()
    {
        $post = Post::factory()->create();
        $response = $this->get($this->getIndexRoute());
        $response->assertRedirect('/login');
    }
    public function getIndexRoute()
    {
        return route('posts.index');
    }
}
