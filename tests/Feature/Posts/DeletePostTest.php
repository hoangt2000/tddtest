<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_delete_post()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $response = $this->delete($this->getDestroyRoute($post->id));
        $this->assertDatabaseMissing('posts', ['id' => $post->id]);
        $response->assertRedirect(route('posts.index'));
    }
    /** @test */
    public function unauthenticate_user_can_not_delete_post()
    {
        $post = Post::factory()->create();
        $response = $this->delete($this->getDestroyRoute($post->id));
        $response->assertRedirect('/login');
    }
    public function getDestroyRoute($id)
    {
        return route('posts.destroy', $id);
    }
}
