<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_update_post()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $dataUpdate = Post::factory()->make()->toArray();
        $response = $this->put($this->getUpdateRoute($post->id), $dataUpdate);
        $this->assertDatabaseHas('posts', $dataUpdate);
        $updatedPost = Post::findOrFail($post->id);
        $this->assertEquals($dataUpdate, ['name' => $updatedPost->name, 'content' => $updatedPost->content]);
        $response->assertRedirect($this->getShowRoute($post->id));
    }
    /** @test */
    public function unauthenticate_user_can_not_uodate_post()
    {
        $post = Post::factory()->create();
        $dataUpdate = Post::factory()->make()->toArray();
        $response = $this->put($this->getUpdateRoute($post->id), $dataUpdate);
        $this->assertNotEquals($dataUpdate, ['name' => $post->name, 'content' => $post->content]);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticate_user_can_not_uodate_post_if_post_not_exits()
    {
        $this->actingAs(User::factory()->create());
        $postId = -1;
        $dataUpdate = Post::factory()->make()->toArray();
        $response = $this->put($this->getUpdateRoute($postId), $dataUpdate);
        $response->assertStatus(404);
    }
    /** @test */
    public function authenticate_user_can_not_uodate_post_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $dataUpdate = Post::factory()->make(['name' => null])->toArray();
        $response = $this->put($this->getUpdateRoute($post->id), $dataUpdate);
        $response->assertSessionHasErrors('name');
    }
    /** @test */
    public function authenticate_user_can_not_uodate_post_if_content_is_null()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $dataUpdate = Post::factory()->make(['content' => null])->toArray();
        $response = $this->put($this->getUpdateRoute($post->id), $dataUpdate);
        $response->assertSessionHasErrors('content');
    }
    public function getUpdateRoute($id)
    {
        return route('posts.update', $id);
    }
    public function getShowRoute($id)
    {
        return route('posts.show', $id);
    }
}
