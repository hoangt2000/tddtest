<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_create_post()
    {
        $this->actingAs(User::factory()->create());
        $dataCreate = Post::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $this->assertDatabaseHas('posts', $dataCreate);
        $post = Post::where($dataCreate)->firstOrFail();
        $response->assertRedirect($this->getShowRoute($post->id));
    }
    /** @test */
    public function unauthenticate_user_can_not_create_post()
    {
        $dataCreate = Post::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertRedirect('/login');
    }
    /** @test */
    public function authenticate_user_can_not_create_post_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $dataCreate = Post::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertSessionHasErrors('name');
    }
    /** @test */
    public function authenticate_user_can_not_create_post_if_content_is_null()
    {
        $this->actingAs(User::factory()->create());
        $dataCreate = Post::factory()->make(['content' => null])->toArray();
        $response = $this->post($this->getStoreRoute(), $dataCreate);
        $response->assertSessionHasErrors('content');
    }
    public function getStoreRoute()
    {
        return route('posts.store');
    }
    public function getShowRoute($id)
    {
        return route('posts.show', $id);
    }
}
