<?php

namespace App\Http\Controllers;

use App\Http\Requests\Posts\CreatePostRequest;
use App\Http\Requests\Posts\UpdatePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $post;
    public function __construct(Post $post)
    {
        $this->post =$post;
    }
    public function show($id)
    {
        $post = $this->post->findOrFail($id);
        return view('posts.show', compact('post'));
    }
    public function update(UpdatePostRequest $request, $id)
    {
        $post = $this->post->findOrFail($id);
        $post->update($request->all());
        return redirect(route('posts.show', $post->id));
    }
    public function index()
    {
        $posts = $this->post->all();
        return view('posts.index', compact('posts'));
    }
    public function store(CreatePostRequest $request)
    {
        $post = $this->post->create($request->all());
        return redirect(route('posts.show', $post->id));
    }
}
