@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p>{{ $task->name }}</p>
                    </div>
                    <div class="card-body">
                        <p>{{ $task->content }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection