@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>Update Task</h2>
                <form action="{{ route('tasks.update', $task->id)}}" method="post">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <input type="text" class="form-group" name="name" value="{{$task->name}}">
                            @error('name')
                                {{ $message }}
                            @enderror
                        </div>
                        <div class="card-body">
                            <input type="text" class="form-group" name="content" value="{{$task->content}}">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection