@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-inverse table-responsive">
                <thead class="thead-inverse">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Content</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr>
                                <td scope="row">{{ $task->id }}</td>
                                <td>{{ $task->name }}</td>
                                <td>{{ $task->content }}</td>
                                <td>
                                    <a name="" id="" class="btn btn-primary" href="tasks/edit/{{$task->id}}" role="button">Update</a>
                                    <form action="{{ route('tasks.delete', $task->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
            </table>
            {{ $tasks->links() }}
        </div>
    </div>
@endsection